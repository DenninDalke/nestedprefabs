namespace NestedPrefabs
{
    using System;
    using UnityEngine;

    public partial class NestedPrefab : MonoBehaviour
    {
        public GameObject Prefab;

        #if !UNITY_EDITOR
        private void Awake()
        {
            this.CreateInstance();
        }
        #endif

        private void CreateInstance()
        {
            if (Prefab == null) return;

            _instantiatedPrefab = Instantiate(Prefab);
            _instantiatedPrefab.transform.SetParent(this.transform, false);
        }
    }
}

#if UNITY_EDITOR
namespace NestedPrefabs
{
    using Core;
    using UnityEngine;
    using Utils;

    [ExecuteInEditMode]
    public partial class NestedPrefab
    {
        [SerializeField]
        private bool _disableNestedPrefabs;

        private GameObject _instantiatedPrefab;
        private GameObject _prefabRoot;
        private GameObject _prefabRootParent;

        public bool IsRootNestedPrefab
        {
            get
            {
                return transform.parent == null || transform.GetComponentsInParent<NestedPrefab>(true, true).Length == 0;
            }
        }

        private bool PreventRun
        {
            get { return _disableNestedPrefabs || NestedPrefabsCoordinator.IsRunning || PrefabUpdater.IsUpdatingPrefab || Application.isPlaying; }
        }

        private void Awake()
        {
            if(Application.isPlaying)
            {
                this.CreateInstance();
                return;
            }

            if (this.PreventRun) return;

            if (NestedPrefabsCoordinator.OnAwakeQueue.Contains(this))
            {
                return;
            }

            //Debug.LogFormat(gameObject, "Awake: {0}", name);

            if (!this.IsRootNestedPrefab) return;

            NestedPrefabsCoordinator.OnAwakeQueue.AddAndCompleteDelayed(this, gameObject.GetPrefabRoot());
        }

        public void CreateInstanceAndReconnect()
        {
            this.CreateInstance();
            this._instantiatedPrefab = PrefabUpdater.ConnectGameObjectToPrefab(_instantiatedPrefab, Prefab);

            var innerNestedPrefabs = _instantiatedPrefab.GetComponentsInChildren<NestedPrefab>(true);
            foreach (var innerNestedPrefab in innerNestedPrefabs)
            {
                if (innerNestedPrefab != this)
                {
                    innerNestedPrefab.CreateInstanceAndReconnect();
                }
            }
        }

        public bool SanitizeChildren()
        {
            if(this.transform.childCount == 0) return false;

            //Debug.LogFormat(gameObject, "SanitizeChildren: {0}, count={1}", name, transform.childCount);

            int childCount = this.transform.childCount - 1;
            for (int i = childCount; i >= 0; i--)
            {
                var child = transform.GetChild(i);
                DestroyImmediate(child.gameObject);
            }

            return true;
        }
    }
}
#endif
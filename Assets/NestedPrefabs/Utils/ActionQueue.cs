namespace NestedPrefabs.Utils
{
    using System;
    using System.Collections.Generic;
    using UnityEditor;

    public class ActionQueue<TKey, TResult> : ActionQueue<TKey>
    {
        public HashSet<TResult> Results { get; private set; }
        
        public ActionQueue(Action finishAction) : base(finishAction)
        {
            Results = new HashSet<TResult>();
        }

        public void AddAndCompleteDelayed(TKey item, TResult result)
        {
            Add(item);
            EditorApplication.delayCall += () => Complete(item, result);
        }

        public  void Complete(TKey item, TResult result)
        {
            Results.Add(result);
            base.Complete(item);
        }

        public override void Clear()
        {
            base.Clear();
            Results.Clear();
        }
    }

    public class ActionQueue<TKey>
    {
        public HashSet<TKey> ActiveQueue { get; private set; }
        public HashSet<TKey> ProcessedQueue { get; private set; }

        private readonly Action FinishAction;

        public ActionQueue(Action finishAction)
        {
            ActiveQueue = new HashSet<TKey>();
            ProcessedQueue = new HashSet<TKey>();
            FinishAction = finishAction;
        }

        public void Add(TKey item)
        {
            ActiveQueue.Add(item);
        }

        public void AddAndCompleteDelayed(TKey item)
        {
            Add(item);
            EditorApplication.delayCall += () => Complete(item);
        }

        public bool Contains(TKey item)
        {
            return ActiveQueue.Contains(item) || ProcessedQueue.Contains(item);
        }

        public virtual void Complete(TKey item)
        {
            ActiveQueue.Remove(item);
            ProcessedQueue.Add(item);

            if (ActiveQueue.Count > 0) return;

            FinishAction();
        }

        public virtual void Clear()
        {
            ActiveQueue.Clear();
            ProcessedQueue.Clear();
        }
    }
}
namespace NestedPrefabs.Utils
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEditor;
    using UnityEngine;

    public static class HierarchyExtension
    {
        public static T[] GetComponentsInParent<T>(this Transform transform, bool includeInactive, bool skipSelf) where T : Component
        {
            return transform.GetComponentsInParent<T>(includeInactive)
                .Where(component => component.transform != transform)
                .ToArray();
        }
    }
}

namespace NestedPrefabs.Utils
{
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;

    public static class PrefabExtension
    {
        public static GameObject GetPrefabRoot(this GameObject gameObject)
        {
            return PrefabUtility.FindRootGameObjectWithSameParentPrefab(gameObject);
        }

        public static GameObject GetPrefabParentRoot(this GameObject gameObject)
        {
            GameObject parentPrefab = (GameObject)PrefabUtility.GetPrefabParent(gameObject);

            if(parentPrefab == null) return null;

            return PrefabUtility.FindRootGameObjectWithSameParentPrefab(parentPrefab);
        }

        public static bool IsPrefabSource(this GameObject gameObject)
        {
            return PrefabUtility.GetPrefabType(gameObject) == PrefabType.Prefab;
        }

        public static T[] GetComponentsInChildrenInSamePrefab<T>(this GameObject gameObject) where T : Component
        {
            var componentRoot = gameObject.GetPrefabRoot();
            var resultComponents = new List<T>();
            var childrenComponents = gameObject.GetComponentsInChildren<T>(true);

            for (int index = 0; index < childrenComponents.Length; index++)
            {
                var childComponent = childrenComponents[index];
                var childRootPrefab = childComponent.gameObject.GetPrefabRoot();

                if (componentRoot != childRootPrefab) continue;

                resultComponents.Add(childComponent);
            }

            return resultComponents.ToArray();
        }
    }
}


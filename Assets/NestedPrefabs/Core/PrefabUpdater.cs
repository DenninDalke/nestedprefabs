namespace NestedPrefabs.Core
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEditor;
    using UnityEngine;
    using Utils;

    [InitializeOnLoad]
    public static class PrefabUpdater
    {
        private delegate T BypassDelegate<T>();

        private static T Bypass<T>(BypassDelegate<T> action)
        {
            var previousState = IsUpdatingPrefab;
            IsUpdatingPrefab = true;
            var result = action();
            IsUpdatingPrefab = previousState;
            return result;
        }

        public static bool IsUpdatingPrefab { get; private set; }

        static PrefabUpdater()
        {
            PrefabUtility.prefabInstanceUpdated += OnPrefabInstanceUpdate;
        }

        #region Bypass methods:
        public static GameObject ReplacePrefab(GameObject instance, GameObject source)
        {
            return Bypass(() => PrefabUtility.ReplacePrefab(instance, source));
        }

        public static GameObject ConnectGameObjectToPrefab(GameObject instance, GameObject source)
        {
            return Bypass(() => PrefabUtility.ConnectGameObjectToPrefab(instance, source));
        }

        public static bool ResetPrefab(Object instance)
        {
            return Bypass(() => PrefabUtility.ResetToPrefabState(instance));
        }

        public static bool ReconnectToLastPrefab(GameObject instance)
        {
            return Bypass(() => PrefabUtility.ReconnectToLastPrefab(instance));
        }

        public static void ReplaceAndReconnect(GameObject instance, GameObject source)
        {
            ReplacePrefab(instance, source);
            ReconnectToLastPrefab(instance);
        }
        #endregion

        private static void OnPrefabInstanceUpdate(GameObject instance)
        {
            if(NestedPrefabsCoordinator.IsRunning) return;

            PrefabUtility.prefabInstanceUpdated -= OnPrefabInstanceUpdate;

            //Debug.LogFormat(instance, "OnPrefabInstanceUpdate: {0}", instance.name);

            NestedPrefabsCoordinator.SanitizeQueue.AddAndCompleteDelayed(instance, instance.GetPrefabParentRoot());

            PrefabUtility.prefabInstanceUpdated += OnPrefabInstanceUpdate;
        }
    }
}


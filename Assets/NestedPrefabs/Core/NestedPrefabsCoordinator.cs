namespace NestedPrefabs.Core
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEditor;
    using UnityEngine;
    using Utils;

    [InitializeOnLoad]
    public static class NestedPrefabsCoordinator
    {
        public static bool IsRunning;

        public static readonly ActionQueue<NestedPrefab, GameObject> OnAwakeQueue;
        public static readonly ActionQueue<GameObject, GameObject> SanitizeQueue;

        static NestedPrefabsCoordinator()
        {
            OnAwakeQueue = new ActionQueue<NestedPrefab, GameObject>(OnAwakeQueueFinish);
            SanitizeQueue = new ActionQueue<GameObject, GameObject>(OnSanitizeQueueFinish);
        }

        private static void OnAwakeQueueFinish()
        {
            bool isSanitized = false;
            IsRunning = true;

            // Remove all children from nested prefabs containers:
            foreach (var nestedPrefab in OnAwakeQueue.ProcessedQueue)
            {
                isSanitized = nestedPrefab.SanitizeChildren() || isSanitized;
            }

            if (isSanitized) ReplaceAndReconnectRootGameObjects();
                
            CreateNestedPrefabInstancesForRootGameObjects(OnAwakeQueue.Results);

            OnAwakeQueue.Clear();
            IsRunning = false;
        }

        private static void ReplaceAndReconnectRootGameObjects()
        {
            foreach (var rootGameObject in OnAwakeQueue.Results)
            {
                //Debug.LogFormat(rootGameObject, "ReplaceAndReconnectRootGameObjects: {0}", rootGameObject.name);
                PrefabUpdater.ReplaceAndReconnect(rootGameObject, rootGameObject.GetPrefabParentRoot());
            }
        }

        private static void CreateNestedPrefabInstancesForRootGameObjects(HashSet<GameObject> gameObjects)
        {
            foreach (var rootGameObject in gameObjects)
            {
                var topMostNestedPrefabs = rootGameObject.GetComponentsInChildren<NestedPrefab>(true);
                InstantiateNestedPrefabs(topMostNestedPrefabs);
            }
        }

        private static void InstantiateNestedPrefabs(NestedPrefab[] nestedPrefabs)
        {
            foreach (NestedPrefab nestedPrefab in nestedPrefabs)
            {
                nestedPrefab.CreateInstanceAndReconnect();
            }
        }

        private static void OnSanitizeQueueFinish()
        {
            IsRunning = true;
            //Debug.LogFormat("OnFinishQueuingRootGameObjectsToSanitize");

            foreach (var updatedInstance in SanitizeQueue.ProcessedQueue)
            {
                SanitizeChildrenNestedPrefabsOf(updatedInstance);
            }

            foreach (var prefabParentRoot in SanitizeQueue.Results)
            {
                var nestedPrefabsInParentRoot = prefabParentRoot.GetComponentsInChildren<NestedPrefab>();

                // If the Parent prefab has invalid children, choose any active instance to update it
                if(nestedPrefabsInParentRoot.Any((nestedPrefab) => nestedPrefab.transform.childCount > 0))
                {
                    //Debug.LogFormat(prefabParentRoot, "Should sanitize this: {0}", prefabParentRoot.name);

                    var firstInstance = SanitizeQueue.ProcessedQueue.First();
                    //Debug.LogFormat(firstInstance, "ReplaceAndReconnectRootGameObjects: {0}", firstInstance.name);
                    PrefabUpdater.ReplaceAndReconnect(firstInstance, prefabParentRoot);
                }
            }

            foreach (var updatedInstance in SanitizeQueue.ProcessedQueue)
            {
                PrefabUpdater.ReconnectToLastPrefab(updatedInstance);
            }
            CreateNestedPrefabInstancesForRootGameObjects(SanitizeQueue.ProcessedQueue);

            SanitizeQueue.Clear();
            IsRunning = false;
        }

        private static void SanitizeChildrenNestedPrefabsOf(GameObject gameObject)
        {
            var nestedPrefabs = gameObject.GetComponentsInChildren<NestedPrefab>(true);
            foreach (var nestedPrefab in nestedPrefabs)
            {
                try
                {
                    nestedPrefab.SanitizeChildren();
                }
                catch(MissingReferenceException)
                {
                    continue;
                }
            }
        }
    }
}